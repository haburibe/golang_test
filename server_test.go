package server

import (
	"testing"
)

func TestGet(t *testing.T) {
	filename := "index.html"
	files := []string{"index.html"}
	got, err := Get(filename, files)
	want := "200 OK"
	if got != want {
		t.Errorf("want %#v, got %#v", want, got)
	}
	if err != nil {
		t.Errorf("unexpected error: %#v", err)
	}
}

func TestGetNotFound(t *testing.T) {
	filename := "index.html"
	files := []string{}
	got, err := Get(filename, files)
	want := "404 Not Found"
	if got != "" {
		t.Errorf("unexpected status: %#v", got)
	}
	if err == nil {
		t.Errorf("want %#v, got %#v", want, err)
	}
	if err != nil && err.Error() != want {
		t.Errorf("want %s, got %#v", want, err.Error())
	}
}
