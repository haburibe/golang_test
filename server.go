package server

import "errors"

func Get(filename string, files []string) (string, error) {
	for _, f := range files {
		if f == filename {
			return "200 OK", nil
		}
	}
	return "", errors.New("404 Not Found")
}
